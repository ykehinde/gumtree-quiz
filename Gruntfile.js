module.exports = grunt => {
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-env');

    const mozjpeg = require('imagemin-mozjpeg');
    const imageminOptipng = require('imagemin-optipng');

    grunt.initConfig({
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'public/img/'
                }],
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [mozjpeg(), imageminOptipng()]
                },
            }
        },
        env: {
            prod: {
                NODE_ENV: 'production'
            }
        },
        watch: {
            images: {
                files:['src/img/*.{png,jpg,gif}'],
                tasks: ['imagemin'],
                options: {
                    spawn: false,
                }
            }
        },
    });

    grunt.registerTask('default', [
        'imagemin',
    ]);

    grunt.registerTask('prod', [
        'env',
        'imagemin',
    ]);
};
