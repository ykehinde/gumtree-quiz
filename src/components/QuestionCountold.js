import React from 'react';
import PropTypes from 'prop-types';

const QuestionCount = ({progress}) => {
  let words = [
    '1','2','3','4','5','6', '7', '8', '9', '10'
  ];
  return (
    <div className="questionCount">
      <h3>{words[progress.counter-1]}</h3>
    </div>
  );
}

QuestionCount.propTypes = {
  progress: PropTypes.object.isRequired
};

export default QuestionCount;
