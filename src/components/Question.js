import React from 'react';
import PropTypes from 'prop-types';

const Question = ({content}) => {
    return (
        <p className="question content-block__paragraph">{content}</p>
    );
}

Question.propTypes = {
    content: PropTypes.string.isRequired
};

export default Question;
