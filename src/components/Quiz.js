import React from 'react';
import PropTypes from 'prop-types';
import Question from '../components/Question';
import AnswerOption from '../components/AnswerOption';
import lifecycle from 'react-pure-lifecycle';

const componentDidMount = () => {  
  var path = 'http://gumtree-jobs.s3-website.eu-west-2.amazonaws.com/img/';

  for (var i = 1; i <= 10; i++) {
    fetch(path + i + 'A.jpg').then((res => res)).catch((err) => console.log(err));
    fetch(path + i + 'B.jpg').then((res => res)).catch((err) => console.log(err));
    fetch(path + i + 'C.jpg').then((res => res)).catch((err) => console.log(err));
  }
};

const methods = {
  componentDidMount
};

const Quiz = ({options}) => {


  function renderAnswerOptions(key) {
    return (
      <AnswerOption
        key={key.key}
        answerLetter={key.key.substring(0, 2)}
        image={progress.counter + key.key}
        answerContent={key.content}
        answerType={key.employer}
        questionId={options.questionId}
        answer={options.answer}
        onAnswerSelected={options.onAnswerSelected}
      />
    );
  }

    let progress = {
      counter: options.questionId,
      total: options.questionTotal
    };

  const QuestionCount = ({progress}) => {
  let words = [
    '1','2','3','4','5','6', '7', '8', '9', '10'
  ];
  return (
    <div className="questionCount">
      <h3>{words[progress.counter-1]} of {words[progress.total-1]}</h3>
    </div>
  );
}

QuestionCount.propTypes = {
  progress: PropTypes.object.isRequired
};

  return (
    <div key={options.questionId}>
      <div className="question-container">
        <QuestionCount progress={progress} />
        <Question content={options.question} />
      </div>
      <ul className="answerOptions clearfix">
        {options.answerOptions.map(renderAnswerOptions)}
      </ul>
    </div>
  );
}

Quiz.propTypes = {
  options: PropTypes.object.isRequired
};

export default lifecycle(methods)(Quiz);
