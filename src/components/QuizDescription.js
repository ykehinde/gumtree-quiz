import React from 'react';
import translations from './config/translations';
import ContentBlockTitle from './ContentBlock__title';
import ContentBlockParagraph from './ContentBlock__paragraph';

const QuizDescription = () => {
    return (
        <div>
            <ContentBlockTitle
            title={translations.Quiz.title}
            subtitle={''} />
            <ContentBlockParagraph paragraph={translations.Quiz.paragraph} />
            <p className="content-block__terms">{translations.Quiz.terms} <br/><a href={translations.Quiz.conditionsLink} target="_blank">{translations.Quiz.conditions}</a></p>
        </div>
    );
}

export default QuizDescription;
