import React from 'react';
import questions from './helpers/questions';
import update from 'immutability-helper';
import Quiz from './Quiz';
import Result from './Result';

const gtag = window.gtag;

class QuizContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            counter: 0,
            questionId: 1,
            question: '',
            answerOptions: [],
            key: '',
            answer: '',
            results: [],
            id: '',
            quizComplete: false
        };

        this.handleAnswerSelected = this.handleAnswerSelected.bind(this);
        this.handleResetQuiz = this.handleResetQuiz.bind(this);
    }

    componentWillMount() {
        this.setState({
            question: questions[0].question,
            answerOptions: questions[0].answers,
        });       
    }

    handleAnswerSelected(event) {
        this.setUserAnswer(event.currentTarget.value);
        
            gtag('event', event.currentTarget.value, {
                'send_to': 'UA-106750260-2',
                'event_category': 'Answer',
            });

        setTimeout(() => {
            if (this.state.questionId >= questions.length) {
                this.setState({
                    quizComplete: true
                })
                return;
            }

            this.setNextQuestion();
        }, 300);
    }

    handleResetQuiz() {
        this.setState({
            counter: 0,
            questionId: 1,
            answer: '',
            results: [],
            question: questions[0].question,
            answerOptions: questions[0].answers,
            quizComplete: false
        });
        this.renderQuiz();
    }

    setUserAnswer(answer) {
        const updatedResults = update(this.state.results, {
            $push: [answer]
        });

        this.setState({
            results: updatedResults,
            answer: answer
        });
    }

    setNextQuestion() {
        let counter =this.state.counter, questionId = this.state.questionId;
            counter = this.state.counter + 1;
            questionId = this.state.questionId + 1;
            
        this.setState({
            counter: counter,
            questionId: questionId,
            question: questions[counter].question,
            answerOptions: questions[counter].answers,
            answer: '',
        });
    }

    getResults() {
        return this.state.results.length;
    }

    renderQuiz() {
        let params = {
            answer: this.state.answer,
            answerOptions: this.state.answerOptions,
            questionId: this.state.questionId,
            question: this.state.question,
            questionTotal: questions.length,
            onAnswerSelected: this.handleAnswerSelected
        };

        return (
            <Quiz options={params} />
        );
    }

    renderResult() {
        const check = ["Deliveroo", "Task Rabbit", "Metropolitan Pubs"],
            results = this.state.results;

        let hTotal = {
            employer: "",
            weight: 0
        };

        for (var i = 0; i < check.length; i++) {
            var count = 0;
            for (var z = 0; z < results.length; z++) {
                if (results[z] === check[i]) count++;
            }
            if (count > hTotal.weight) {
                hTotal.employer = check[i];
                hTotal.weight = count;
            }
        }

        let resultNumber;

        switch (hTotal.employer) {
            case 'Deliveroo':
                resultNumber = 0;
                break;
            case 'Task Rabbit':
                resultNumber = 1;
                break;
            case 'Metropolitan Pubs':
                resultNumber = 2;
                break;
            default:
                return hTotal.employer;
        }        

        let params = {
            employerIndex: resultNumber,
            onResetQuiz: this.handleResetQuiz,
        };
        return (
            <Result options={params}/>
        );
    }

    render() {
        // eslint-disable-next-line
        let component = !this.state.quizComplete && this.renderQuiz()
        // eslint-disable-next-line
        || this.state.quizComplete && this.renderResult();        

        return (
            <div className="content-block quiz">
                <div className={`content-block--contain clearfix`}>
                    <div className="content-block--separator">
                        {component}
                    </div>
                </div>
            </div>
        );
    }
}

export default QuizContainer;
