import React from 'react';
import PropTypes from 'prop-types';
import resultOptions from './helpers/results';

const gtag = window.gtag;

const Result = ({options}) => {
  let index = options.employerIndex;
  let reset = options.onResetQuiz;
  
  gtag('event', resultOptions[index].result, {
    'send_to': 'UA-106750260-2',
    'event_category': 'Result',
  });
  
  return (
    <div className="resultsPage">
      <img className="resultImage" src={`../img/${resultOptions[index].image}`} alt ={resultOptions[index].result} />
      <div className="resultText">
        {resultOptions[index].context.map((paragraph, i) => 
          <p key={i} className="resultsContext">{paragraph}</p>
        )}
      </div>
      <a className="resultsButton" href={resultOptions[index].url} target="_blank">Explore Jobs</a>
      <a className="resultsButton" onClick={reset} target="_blank">Take the quiz again</a>
    </div>

  );
}

Result.propTypes = {
  options: PropTypes.object.isRequired
};

export default Result;


