import React from 'react';
import PropTypes from 'prop-types';

function AnswerOption(props) {
  var background = {
    width: "100%",
    height: "100px"
  }
  
  return (
    
    <li className={`answerOption`} style={background}>
    <img className="answerImage" src={`../img/${props.image}.jpg`} alt=""/>
      <h2>{props.answerLetter}</h2>
      <input
        type="radio"
        name="radioGroup"
        checked={props.employer === props.answer}
        id={props.answerType}
        value={props.answerType}
        disabled={props.answer}
        onChange={props.onAnswerSelected}
      />
      <label className="radioCustomLabel" htmlFor={props.answerType}>
        {props.answerContent}
      </label>
    </li>
  );
}

AnswerOption.propTypes = {
  answerType: PropTypes.string.isRequired,
  answerContent: PropTypes.string.isRequired,
  answer: PropTypes.string.isRequired,
  onAnswerSelected: PropTypes.func.isRequired
};

export default AnswerOption;
