let results = [
    {
        result: "Deliveroo",
        context: [
            "You got Deliveroo driver! A cool and young company, Deliveroo drivers are a crucial part of this food delivery service.", 
            "You’re a cool-headed person that handles themselves well under pressure, so keeping up with Deliveroo’s excellent service standards of fast and reliable delivery will be a walk in the park (or a cycle in the city) for you.", 
            "You’re driven and self-motivated, exactly what’s needed as Deliveroo’s drivers enjoy the benefits of being self-employed."
        ],
        url: "https://www.gumtree.com/jobs/employer/1395728295/deliveroo-/",
        image: "Deliveroo_Header.jpg"
    },
    {
        result: "Task Rabbit",
        context: [
            "You got Task Rabbit! You’re independent and a real go-getter. You would thrive being your own boss and setting your own rates and hours – the perfect match for becoming a Tasker on Task Rabbit.", 
            "Whether it’s helping assemble a baby cot, or clearing the garden of someone’s new home, you’re can turn your hand to pretty much anything."
        ],
        url: "https://www.gumtree.com/jobs/employer/61841299/taskrabbit-limited/?LinkSource=HomePage",
        image: "TaskRabbit_Header.jpg"
    },
    {
        result: "The Metropolitan Pubs",
        context: [
            "You got The Metropolitan Pub Company! You’re a confident people person that thrives when things get busy - the perfect match for handling busy shifts when working with the team at The Metropolitan Pub Company.", 
            "Your warm and friendly personality goes hand in hand with meeting the excellent customer service standards here."
        ],
        url: "https://www.gumtree.com/jobs/minisites/mpco/b/",
        image: "MPCo_Header.jpg"
    },
];

export default results;