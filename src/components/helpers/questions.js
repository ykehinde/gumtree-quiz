let quizQuestions = [
    {
    question: "I would describe myself as…",
    answers: [
        {
            key: "A",
            content: "Self-motivated",
            employer: "Deliveroo",
            background: "1A"
        },
        {
            key: "B",
            content: "Empathetic",
            employer: "Task Rabbit",
            background: "1B"
        },
        {
            key: "C",
            content: "Confident",
            employer: "Metropolitan Pubs",
            background: "1C"
        }
    ]
},
{
    question: "My ideal evening would be…",
    answers: [
        {
            key: "A",
            content: "Going to a gig",
            employer: "Metropolitan Pubs",
            background: "2A"
        },
        {
            key: "B",
            content: "Hanging with pals",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "C",
            content: "Hitting the gym",
            employer: "Deliveroo",
            background: ""
        }
    ]
},
{
    question: "If I was a dog, I would be a…",
    answers: [
        {
            key: "A",
            content: "Labrador – loyal and loving",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "B",
            content: "Jack Russel – inquisitive and lively",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "C",
            content: "Pug – charming yet mischievous",
            employer: "Metropolitan Pubs",
            background: ""
        }
    ]
},
{
    question: "Which one is most true for you…",
    answers: [
        {
            key: "A",
            content: "I love to be outdoors",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "B",
            content: "I like to be busy",
            employer: "Metropolitan Pubs",
            background: ""
        },
        {
            key: "C",
            content: "I love to travel",
            employer: "Task Rabbit",
            background: ""
        }
    ]
},
{
    question: "I could never live without my…",
    answers: [
        {
            key: "A",
            content: "Phone",
            employer: "Metropolitan Pubs",
            background: ""
        },
        {
            key: "B",
            content: "Car",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "C",
            content: "Cheese",
            employer: "Task Rabbit",
            background: ""
        }
    ]
},
{
    question: "I am most productive… ",
    answers: [
        {
            key: "A",
            content: "In the morning",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "B",
            content: "In the afternoon",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "C",
            content: "In the evening",
            employer: "Metropolitan Pubs",
            background: ""
        }
    ]
},
{
    question: "I prefer working…",
    answers: [
        {
            key: "A",
            content: "On my own",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "B",
            content: "In a small team",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "C",
            content: "In a large team",
            employer: "Metropolitan Pubs",
            background: ""
        }
    ]
},
{
    question: "Pick a Disney villain…",
    answers: [
        {
            key: "A",
            content: "Cruella De Vil",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "B",
            content: "Scar",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "C",
            content: "Maleficent",
            employer: "Metropolitan Pubs",
            background: ""
        }
    ]
},
{
    question: "People say that I am…",
    answers: [
        {
            key: "A",
            content: "The chatterbox",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "B",
            content: "The creative",
            employer: "Metropolitan Pubs",
            background: ""
        },
        {
            key: "C",
            content: "The free spirit",
            employer: "Deliveroo",
            background: ""
        }
    ]
},
{
    question: "If I could have one superpower, it would be…",
    answers: [
        {
            key: "A",
            content: "The ability to fly ",
            employer: "Deliveroo",
            background: ""
        },
        {
            key: "B",
            content: "Superhuman strength",
            employer: "Task Rabbit",
            background: ""
        },
        {
            key: "C",
            content: "Invisibility",
            employer: "Metropolitan Pubs",
            background: ""
        }
    ]
}
];

export default quizQuestions;


